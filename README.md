## Icecast Container Registry

This GitLab repository serves as a container registry for Icecast images.

### Pulling Images

Images can be pulled from this registry using:

`docker pull registry.gitlab.com/vito-containers/icecast:$TAGNAME`

Replace `$TAGNAME` with one of the available tags:

### Tags

- [`latest`](https://gitlab.com/vito-containers/icecast-builds/icecast/-/blob/main/containers/2.4/Dockerfile),
  [`2.4`](https://gitlab.com/vito-containers/icecast-builds/icecast/-/blob/main/containers/2.4/Dockerfile),
  [`2.4.4`](https://gitlab.com/vito-containers/icecast-builds/icecast/-/blob/main/containers/2.4/Dockerfile)

### Build Projects

The following projects can build and push images to this registry:

- [icecast](https://gitlab.com/vito-containers/icecast-builds/icecast/) - Main Icecast build
